// Import Libraries
import Vue from 'vue'
import VueRouter from 'vue-router';
import { routes } from './routes';
import { store } from './store/store';

// Import Components
import App from './App.vue'

// Register Vue Plugins
Vue.use(VueRouter);

// Configure Vue Router
const router = new VueRouter({
  routes,
  mode: 'history'
});


// Root Vue Instance
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
