// Import Libraries
import Vue from 'vue';
import Vuex from 'vuex';

// Register Vuex
Vue.use(Vuex);

// Configure Vuex Store
export const store = new Vuex.Store({
    state: {
        baseAPI : 'http://wpscouts.lvh.me:9000/api/v1',
        authToken: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU5ZTU0YmM0ODg3MzJkNGM4MDNkNGY4ZCIsImlhdCI6MTUwODE5OTM3NH0.aQz9YJgJi1xeLg-Jv7_6Me0vlHugqY8ZsGL2q1uwonM',
        siteURL: '#'
    },
    getters: {
        baseAPI: state => {
            return state.baseAPI;
        },
        authToken: state => {
            return state.authToken;
        },
    	siteURL: state => {
    		return state.siteURL;
    	}
    },
    mutations: {
    	setSiteURL: (state, payload) => {
    		state.siteURL = payload;
    	}
    },
    actions: {
    	setSiteURL: ({commit}, payload) => {
    		commit('setSiteURL', payload);
    	}
    }
});