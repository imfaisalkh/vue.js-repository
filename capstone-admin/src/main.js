// Import Libraries
import Vue from 'vue'
import router from './router'
import { store } from './store/store'

// Import Components
import App from './App'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
