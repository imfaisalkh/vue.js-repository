// Import Modules
import Vue from 'vue'
import Router from 'vue-router'

// Import Components
import Home from '@/components/Home.vue'
import Login from '@/components/login/Login.vue'
import Register from '@/components/register/Register.vue'

import AdminPanel from '@/components/admin/AdminPanel.vue'

import Projects from '@/components/admin/projects/Projects.vue'
import Pages from '@/components/admin/pages/Pages.vue'
import Themes from '@/components/admin/themes/Themes.vue'
import Customize from '@/components/admin/customize/Customize.vue'

import Profile from '@/components/admin/profile/Profile.vue'
import Settings from '@/components/admin/settings/Settings.vue'
import Billing from '@/components/admin/billing/Billing.vue'
import Usage from '@/components/admin/usage/Usage.vue'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/login', name: 'login', component: Login },
    { path: '/register', name: 'register', component: Register },

    { path: '/admin',
      name: 'admin',
      component: AdminPanel,
      children: [
        { path: 'projects', name: 'projects', component: Projects, meta: { title: 'Projects' } },
        { path: 'pages', name: 'pages', component: Pages, meta: { title: 'Pages' } },
        { path: 'themes', name: 'themes', component: Themes, meta: { title: 'Themes' } },
        { path: 'customize', name: 'customize', component: Customize },

        { path: 'profile', name: 'profile', component: Profile, meta: { title: 'Profile' } },
        { path: 'settings', name: 'settings', component: Settings, meta: { title: 'Settings' } },
        { path: 'billing', name: 'billing', component: Billing, meta: { title: 'Billing' } },
        { path: 'usage', name: 'usage', component: Usage, meta: { title: 'Usage Statistics' } }
      ]
    }
  ]
})
