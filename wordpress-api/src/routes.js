// Import Components
import Dashboard from './components/Dashboard.vue'
import Error404 from './components/Error404.vue'
import Posts from './components/posts/Posts.vue'
import Pages from './components/pages/Pages.vue'
import Themes from './components/themes/Themes.vue'
import Settings from './components/settings/Settings.vue'


export const routes = [
    { path: '', name: 'home', component: Dashboard },
    { path: '/settings', name: 'settings', component: Settings },
    { path: '/themes', name: 'themes', component: Themes },
    { path: '/pages', name: 'pages', component: Pages },
    { path: '/posts', component: Posts,
        children: [
            { path: '', name: 'posts', component: Posts }
        ]
    },
    { path: '*', name: '404', component: Error404 }
];