// Import Libraries
import Vue from 'vue';
import Vuex from 'vuex';

// Register Vuex
Vue.use(Vuex);

// Configure Vuex Store
export const store = new Vuex.Store({
    state: {
        baseAPI : 'http://demo.wpscouts.net/rest-api/wp-json',
        authAPI: 'Basic d3BzY291dDpnYTI2MjBtYzk2NDRtYw==',
        // baseAPI : 'http://localhost/wordpress/nucleus/wp-json',
        // authAPI: 'Basic YWRtaW46YWRtaW4=',
        siteURL: '#'
    },
    getters: {
        baseAPI: state => {
            return state.baseAPI;
        },
        authAPI: state => {
            return state.authAPI;
        },
    	siteURL: state => {
    		return state.siteURL;
    	}
    },
    mutations: {
    	setSiteURL: (state, payload) => {
    		state.siteURL = payload;
    	}
    },
    actions: {
    	setSiteURL: ({commit}, payload) => {
    		commit('setSiteURL', payload);
    	}
    }
});